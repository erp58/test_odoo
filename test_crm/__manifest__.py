{
    "name": "Test CRM",
    "version": "15.0.0",
    "website": "http://www.test.com",
    "category": "CRM Setting",
    "depends": [
        'crm',
        ],
    "description": """ Test CRM """,
    "demo": [],
    'data': [
        'security/ir.model.access.csv',
        'security/groups.xml',
        'data/action_before_closing_date.xml',
        'views/res_partner_view.xml',
        'views/res_partner_view.xml',
        'views/website_view.xml',
        'wizard/track_wizard_view.xml',
        'reports/print_report_lead.xml',
        'reports/report_lead.xml',
    ],
    'installable': True,
}
