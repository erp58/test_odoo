# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class TrackWizard(models.TransientModel):
    _name = 'track.wizard'
    _description = 'create the track and set a date for the next action'

    create_track = fields.Char(string='create track')

    # Action for showing the second wizard
    def action_set_date(self):
        self.ensure_one()
        view_id = self.env.ref('test_crm.set_date_wizard_form').id
        return {
            'name': _("Set a date for the next action"),
            'type': 'ir.actions.act_window',
            'res_model': 'set.date.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
        }


class SetDate(models.TransientModel):
    _name = 'set.date.wizard'
    _description = 'create the track and set a date for the next action'

    set_date = fields.Datetime(string='Set Date')


