from odoo import models, fields, api, _


class CrmLead(models.Model):
    _inherit = "crm.lead"

    closing_date = fields.Datetime(string="Closing date")

    # Check partner's date automatically
    @api.onchange('partner_id', 'lang_id')
    def _onchange_check_lang(self):
        if self.partner_id:
            self.lang_id = self.partner_id.lang

    # Action for showing the first wizard
    def action_create_track(self):
        self.ensure_one()
        view_id = self.env.ref('test_crm.create_track_wizard_form').id
        return {
            'name': _("create the track and set a date for the next action"),
            'type': 'ir.actions.act_window',
            'res_model': 'track.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
        }
