from odoo import models, fields


class ResPartner(models.Model):
    _inherit = "res.partner"

    current_track = fields.Datetime(string="Current track")
